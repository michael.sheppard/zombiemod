/*
 * AbstractCEZombieModel.java
 *
 *  Copyright (c) 2019 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.zombiemod.client;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nonnull;

@OnlyIn(Dist.CLIENT)
public abstract class AbstractCEZombieModel<T extends MonsterEntity> extends BipedModel<T> {
    protected AbstractCEZombieModel(float size, float scale, int texWidth, int texHeight) {
        super(size, scale, texWidth, texHeight);
    }

    public void setRotationAngles(@Nonnull T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        super.setRotationAngles(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
        boolean hasTarget = isAgressive(entityIn);
        float rightArmRotation = MathHelper.sin(swingProgress * (float) Math.PI);
        float leftArmRotation = MathHelper.sin((1.0F - (1.0F - swingProgress) * (1.0F - swingProgress)) * (float) Math.PI);
        bipedRightArm.rotateAngleZ = 0.0F;
        bipedLeftArm.rotateAngleZ = 0.0F;
        bipedRightArm.rotateAngleY = -(0.1F - rightArmRotation * 0.6F);
        bipedLeftArm.rotateAngleY = 0.1F - rightArmRotation * 0.6F;
        float armAngle = hasTarget ? -((float) Math.PI / 1.5F) : 0.0F;
        bipedRightArm.rotateAngleX = armAngle;
        bipedLeftArm.rotateAngleX = armAngle;
        bipedRightArm.rotateAngleX += rightArmRotation * 1.2F - leftArmRotation * 0.4F;
        bipedLeftArm.rotateAngleX += rightArmRotation * 1.2F - leftArmRotation * 0.4F;
        bipedRightArm.rotateAngleZ += MathHelper.cos(ageInTicks * 0.09F) * 0.05F + 0.05F;
        bipedLeftArm.rotateAngleZ -= MathHelper.cos(ageInTicks * 0.09F) * 0.05F + 0.05F;
        bipedRightArm.rotateAngleX += MathHelper.sin(ageInTicks * 0.067F) * 0.05F;
        bipedLeftArm.rotateAngleX -= MathHelper.sin(ageInTicks * 0.067F) * 0.05F;
    }

    public abstract boolean isAgressive(T entity);
}
