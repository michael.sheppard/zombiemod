/*
 * AbstractCEZombieRenderer.java
 *
 *  Copyright (c) 2019 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.zombiemod.client;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.zombiemod.common.CEZombieEntity;
import net.minecraft.client.renderer.entity.BipedRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.layers.BipedArmorLayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nonnull;

@OnlyIn(Dist.CLIENT)
public class AbstractCEZombieRenderer<T extends CEZombieEntity, M extends CEZombieModel<T>> extends BipedRenderer<T, M> {
    private static final ResourceLocation TEXTURE_LOCATION = new ResourceLocation("textures/entity/zombie/zombie.png");

    protected AbstractCEZombieRenderer(EntityRendererManager rendererManager, M model1, M model2, M model3) {
        super(rendererManager, model1, 0.5F);
        this.addLayer(new BipedArmorLayer<>(this, model2, model3));
    }

    @Nonnull
    public ResourceLocation getEntityTexture(@Nonnull CEZombieEntity entity) {
        return TEXTURE_LOCATION;
    }

    protected void applyRotations(T entityLiving, @Nonnull MatrixStack matrixStack, float ageInTicks, float rotationYaw, float partialTicks) {
        if (entityLiving.isDrowning()) {
            rotationYaw += (float) (Math.cos((double) entityLiving.ticksExisted * 3.25D) * Math.PI * 0.25D);
        }

        super.applyRotations(entityLiving, matrixStack, ageInTicks, rotationYaw, partialTicks);
    }

    @Override
    protected void preRenderCallback(T entity, MatrixStack matrixStack, float f) {
        float scale = entity.getScaleFactor();
        matrixStack.scale(scale, scale, scale);
    }
}
