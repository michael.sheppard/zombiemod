/*
 * CEPigZombieRenderer.java
 *
 *  Copyright (c) 2019 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.zombiemod.client;

import com.zombiemod.common.CEPigZombieEntity;
import net.minecraft.client.renderer.entity.BipedRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.layers.BipedArmorLayer;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;

public class CEPigZombieRenderer extends BipedRenderer<CEPigZombieEntity, CEZombieModel<CEPigZombieEntity>> {
    private static final ResourceLocation ZOMBIE_PIGMAN_TEXTURE = new ResourceLocation("textures/entity/zombie_pigman.png");

    public CEPigZombieRenderer(EntityRendererManager renderManagerIn) {
        super(renderManagerIn, new CEZombieModel<>(), 0.5F);
        this.addLayer(new BipedArmorLayer<>(this, new CEZombieModel<>(0.5F, true), new CEZombieModel<>(1.0F, true)));
    }

    @Nonnull
    public ResourceLocation getEntityTexture(@Nonnull CEPigZombieEntity entity) {
        return ZOMBIE_PIGMAN_TEXTURE;
    }
}
