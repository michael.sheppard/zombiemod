/*
 * CEZombieModel.java
 *
 *  Copyright (c) 2019 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */
package com.zombiemod.client;

import com.zombiemod.common.CEZombieEntity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class CEZombieModel<T extends CEZombieEntity> extends AbstractCEZombieModel<T> {

    @SuppressWarnings("unused")
    public CEZombieModel() {
        this(0.0F, false);
    }

    @SuppressWarnings("unused")
    protected CEZombieModel(float size, float scale, int texWidth, int texHeight) {
        super(size, scale, texWidth, texHeight);
    }

    public CEZombieModel(float size, boolean isChild) {
        super(size, 0.0F, 64, isChild ? 32 : 64);
    }

    @Override
    @SuppressWarnings("unused")
    public boolean isAgressive(T entity) {
        return entity.isAggressive();
    }

}
