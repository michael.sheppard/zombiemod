package com.zombiemod.client;

import com.zombiemod.common.CEHuskEntity;
import net.minecraft.client.renderer.entity.BipedRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;

public class CEHuskRenderer  extends BipedRenderer<CEHuskEntity, CEZombieModel<CEHuskEntity>> {
    private static final ResourceLocation CEHUSK_TEXTURES = new ResourceLocation("textures/entity/zombie/husk.png");

    public CEHuskRenderer(EntityRendererManager renderManagerIn) {
        super(renderManagerIn, new CEZombieModel<>(), 0.5f);
    }

    @Nonnull
    public ResourceLocation getEntityTexture(@Nonnull CEHuskEntity entity) {
        return CEHUSK_TEXTURES;
    }
}
