/*
 * CEPigZombieEntity.java
 *
 *  Copyright (c) 2019 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.zombiemod.common;

import net.minecraft.entity.*;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.ai.goal.ZombieAttackGoal;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.UUID;

public class CEPigZombieEntity extends CEZombieEntity {
    private static final UUID ATTACK_SPEED_BOOST_MODIFIER_UUID = UUID.fromString("49455A49-7EC5-45BA-B886-3B90B23A1718");
    private static final AttributeModifier ATTACK_SPEED_BOOST_MODIFIER = (new AttributeModifier(ATTACK_SPEED_BOOST_MODIFIER_UUID, "Attacking speed boost", 0.05D, AttributeModifier.Operation.ADDITION)).setSaved(false);
    private int angerLevel;
    private int randomSoundDelay;
    private UUID angerTargetUUID;

    public CEPigZombieEntity(EntityType<? extends CEPigZombieEntity> entityType, World world) {
        super(entityType, world);
        setPathPriority(PathNodeType.LAVA, 8.0F);
    }

    @SuppressWarnings("unused")
    public CEPigZombieEntity(World world) {
        this(CEZombieMod.RegistryEvents.CEPIGZOMBIE, world);
    }

    @Override
    public void setRevengeTarget(@Nullable LivingEntity livingBase) {
        super.setRevengeTarget(livingBase);
        if (livingBase != null) {
            angerTargetUUID = livingBase.getUniqueID();
        }
    }

    @Override
    protected void applyEntityAI() {
        goalSelector.addGoal(2, new ZombieAttackGoal(this, 1.0D, false));
        this.goalSelector.addGoal(7, new WaterAvoidingRandomWalkingGoal(this, 1.0D));
        this.targetSelector.addGoal(1, new HurtByAggressorGoal(this));
        this.targetSelector.addGoal(2, new TargetAggressorGoal(this));
    }

    @Override
    protected void registerAttributes() {
        super.registerAttributes();
        getAttribute(SPAWN_REINFORCEMENTS_CHANCE).setBaseValue(0.0D);
        getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.23F);
        getAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(5.0D);
    }

    @Override
    protected boolean shouldDrown() {
        return false;
    }

    @Override
    protected void updateAITasks() {
        IAttributeInstance iattributeinstance = getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED);
        LivingEntity livingentity = getRevengeTarget();
        if (isAngry()) {
            if (!isChild() && !iattributeinstance.hasModifier(ATTACK_SPEED_BOOST_MODIFIER)) {
                iattributeinstance.applyModifier(ATTACK_SPEED_BOOST_MODIFIER);
            }

            --angerLevel;
            LivingEntity livingentity1 = livingentity != null ? livingentity : getAttackTarget();
            if (!isAngry() && livingentity1 != null) {
                if (!canEntityBeSeen(livingentity1)) {
                    setRevengeTarget(null);
                    setAttackTarget(null);
                } else {
                    angerLevel = getAngerLevel();
                }
            }
        } else if (iattributeinstance.hasModifier(ATTACK_SPEED_BOOST_MODIFIER)) {
            iattributeinstance.removeModifier(ATTACK_SPEED_BOOST_MODIFIER);
        }

        if (randomSoundDelay > 0 && --randomSoundDelay == 0) {
            playSound(SoundEvents.ENTITY_ZOMBIE_PIGMAN_ANGRY, getSoundVolume() * 2.0F, ((rand.nextFloat() - rand.nextFloat()) * 0.2F + 1.0F) * 1.8F);
        }

        if (isAngry() && angerTargetUUID != null && livingentity == null) {
            PlayerEntity playerentity = world.getPlayerByUuid(angerTargetUUID);
            setRevengeTarget(playerentity);
            attackingPlayer = playerentity;
            recentlyHit = getRevengeTimer();
        }

        super.updateAITasks();
    }

    @Override
    public boolean isNotColliding(IWorldReader worldIn) {
        return worldIn.hasNoCollisions(this) && !worldIn.containsAnyLiquid(getBoundingBox());
    }

    @Override
    public void writeAdditional(CompoundNBT compound) {
        super.writeAdditional(compound);
        compound.putShort("Anger", (short) angerLevel);
        if (angerTargetUUID != null) {
            compound.putString("HurtBy", angerTargetUUID.toString());
        } else {
            compound.putString("HurtBy", "");
        }

    }

    @Override
    public void readAdditional(CompoundNBT compound) {
        super.readAdditional(compound);
        angerLevel = compound.getShort("Anger");
        String s = compound.getString("HurtBy");
        if (!s.isEmpty()) {
            angerTargetUUID = UUID.fromString(s);
            PlayerEntity playerentity = world.getPlayerByUuid(angerTargetUUID);
            setRevengeTarget(playerentity);
            if (playerentity != null) {
                attackingPlayer = playerentity;
                recentlyHit = getRevengeTimer();
            }
        }

    }

    @Override
    public boolean attackEntityFrom(@Nonnull DamageSource source, float amount) {
        if (isInvulnerableTo(source)) {
            return false;
        } else {
            Entity entity = source.getTrueSource();
            if (entity instanceof PlayerEntity && !((PlayerEntity) entity).isCreative() && canEntityBeSeen(entity)) {
                becomeAngryAt(entity);
            }

            return super.attackEntityFrom(source, amount);
        }
    }

    private boolean becomeAngryAt(Entity entity) {
        angerLevel = getAngerLevel();
        randomSoundDelay = rand.nextInt(40);
        if (entity instanceof LivingEntity) {
            setRevengeTarget((LivingEntity) entity);
        }

        return true;
    }

    private int getAngerLevel() {
        return 400 + rand.nextInt(400);
    }

    private boolean isAngry() {
        return angerLevel > 0;
    }

    @Override
    protected SoundEvent getAmbientSound() {
        return SoundEvents.ENTITY_ZOMBIE_PIGMAN_AMBIENT;
    }

    @Override
    protected SoundEvent getHurtSound(@Nonnull DamageSource damageSourceIn) {
        return SoundEvents.ENTITY_ZOMBIE_PIGMAN_HURT;
    }

    @Override
    protected SoundEvent getDeathSound() {
        return SoundEvents.ENTITY_ZOMBIE_PIGMAN_DEATH;
    }

    @Override
    public boolean processInteract(@Nonnull PlayerEntity player, @Nonnull Hand hand) {
        return false;
    }

    @Override
    protected void setEquipmentBasedOnDifficulty(@Nonnull DifficultyInstance difficulty) {
        setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(Items.DIAMOND_SWORD));
    }

    @Nonnull
    @Override
    protected ItemStack getSkullDrop() {
        return ItemStack.EMPTY;
    }

    @Override
    public boolean isPreventingPlayerRest(@Nonnull PlayerEntity playerIn) {
        return isAngry();
    }

    static class HurtByAggressorGoal extends HurtByTargetGoal {
        public HurtByAggressorGoal(CEPigZombieEntity p_i45828_1_) {
            super(p_i45828_1_);
            setCallsForHelp(CEZombieEntity.class);
        }

        protected void setAttackTarget(@Nonnull MobEntity mobIn, @Nonnull LivingEntity targetIn) {
            if (mobIn instanceof CEPigZombieEntity && goalOwner.canEntityBeSeen(targetIn) && ((CEPigZombieEntity) mobIn).becomeAngryAt(targetIn)) {
                mobIn.setAttackTarget(targetIn);
            }

        }
    }

    static class TargetAggressorGoal extends NearestAttackableTargetGoal<PlayerEntity> {
        public TargetAggressorGoal(CEPigZombieEntity entity) {
            super(entity, PlayerEntity.class, true);
        }

        public boolean shouldExecute() {
            return ((CEPigZombieEntity) goalOwner).isAngry() && super.shouldExecute();
        }
    }
}
