package com.zombiemod.common;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nonnull;

public class CEHuskEntity extends CEZombieEntity {
    public CEHuskEntity(EntityType<? extends CEZombieEntity> type, World world) {
        super(type, world);
    }

    @SuppressWarnings("unused")
    public CEHuskEntity(World world) {
        this(CEZombieMod.RegistryEvents.CEHUSK, world);
    }

    @Override
    protected boolean shouldBurnInDay() {
        return false;
    }

    @Override
    protected SoundEvent getAmbientSound() {
        return SoundEvents.ENTITY_HUSK_AMBIENT;
    }

    @Override
    protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
        return SoundEvents.ENTITY_HUSK_HURT;
    }

    @Override
    protected SoundEvent getDeathSound() {
        return SoundEvents.ENTITY_HUSK_DEATH;
    }

    @Override
    @Nonnull
    protected SoundEvent getStepSound() {
        return SoundEvents.ENTITY_HUSK_STEP;
    }

    @Override
    public boolean attackEntityAsMob(@Nonnull Entity entityIn) {
        boolean flag = super.attackEntityAsMob(entityIn);
        if (flag && getHeldItemMainhand().isEmpty() && entityIn instanceof LivingEntity) {
            float difficulty = world.getDifficultyForLocation(new BlockPos(this)).getAdditionalDifficulty();
            ((LivingEntity)entityIn).addPotionEffect(new EffectInstance(Effects.HUNGER, 140 * (int)difficulty));
        }

        return flag;
    }

    @Override
    protected boolean shouldDrown() {
        return true;
    }

    @Override
    protected void onDrowned() {
        func_213698_b(CEZombieMod.RegistryEvents.CEZOMBIE);
        world.playEvent(null, 1041, new BlockPos(this), 0);
    }

    @Override
    @Nonnull
    protected ItemStack getSkullDrop() {
        return ItemStack.EMPTY;
    }
}
