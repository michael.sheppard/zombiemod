/*
 * CEZombieMod.java
 *
 *  Copyright (c) 2019 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */
package com.zombiemod.common;


import com.zombiemod.client.CEHuskRenderer;
import com.zombiemod.client.CEPigZombieRenderer;
import com.zombiemod.client.CEZombieRenderer;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.*;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistryEntry;
import net.minecraftforge.registries.ObjectHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.LinkedList;


@SuppressWarnings("unused")
@Mod(CEZombieMod.MODID)
public class CEZombieMod {

    public static final String MODID = "zombiemod";
    public static final String CEZOMBIE_MOD_NAME = " CEZombie Mod";
    public static final String CEZOMBIE_NAME = "cezombie";
    public static final String CEPIGZOMBIE_NAME = "cepigzombie";
    public static final String CEHUSK_NAME = "cehusk";
    public static final String CEZOMBIE_SPAWN_EGG_NAME = "cezombie_spawn_egg";
    public static final String CEPIGZOMBIE_SPAWN_EGG_NAME = "cepigzombie_spawn_egg";
    public static final String CEHUSK_SPAWN_EGG_NAME = "cehusk_spawn_egg";

    private static final Logger LOGGER = LogManager.getLogger(CEZombieMod.MODID);

    public CEZombieMod() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::initClient);

        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, ConfigHandler.commonSpec);

        MinecraftForge.EVENT_BUS.register(this);
    }

    public void setup(final FMLCommonSetupEvent event) {
        ConfigHandler.loadConfig();
        MinecraftForge.EVENT_BUS.register(new com.zombiemod.common.CheckSpawnEvent());

        registerSpawns();
    }

    private void initClient(final FMLClientSetupEvent event) {
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.CEZOMBIE, CEZombieRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.CEPIGZOMBIE, CEPigZombieRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.CEHUSK, CEHuskRenderer::new);
    }

    @Mod.EventBusSubscriber(modid = CEZombieMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
    @ObjectHolder(CEZombieMod.MODID)
    public static class RegistryEvents {
        public final static EntityType<CEZombieEntity> CEZOMBIE = EntityType.Builder.<CEZombieEntity>create(CEZombieEntity::new, EntityClassification.MONSTER)
                .setTrackingRange(80).build(CEZombieMod.MODID);

        public final static EntityType<CEPigZombieEntity> CEPIGZOMBIE = EntityType.Builder.<CEPigZombieEntity>create(CEPigZombieEntity::new, EntityClassification.MONSTER)
                .setTrackingRange(80).build(CEZombieMod.MODID);

        public final static EntityType<CEHuskEntity> CEHUSK = EntityType.Builder.<CEHuskEntity>create(CEHuskEntity::new, EntityClassification.MONSTER)
                .setTrackingRange(80).build(CEZombieMod.MODID);

        @SubscribeEvent
        public static void onEntityRegistry(final RegistryEvent.Register<EntityType<?>> event) {
            event.getRegistry().registerAll(
                    setup(RegistryEvents.CEZOMBIE, CEZombieMod.CEZOMBIE_NAME),
                    setup(RegistryEvents.CEPIGZOMBIE, CEZombieMod.CEPIGZOMBIE_NAME),
                    setup(RegistryEvents.CEHUSK, CEZombieMod.CEHUSK_NAME)
            );
        }

        @SubscribeEvent
        public static void onItemRegistry(final RegistryEvent.Register<Item> event) {
            event.getRegistry().registerAll(
                    setup(new SpawnEggItem(CEZOMBIE, 0x00afaf, 0x799c65, (new Item.Properties()).group(ItemGroup.MISC)), CEZombieMod.CEZOMBIE_SPAWN_EGG_NAME),
                    setup(new SpawnEggItem(CEPIGZOMBIE, 0x799c45, 0x00afaf, (new Item.Properties()).group(ItemGroup.MISC)), CEZombieMod.CEPIGZOMBIE_SPAWN_EGG_NAME),
                    setup(new SpawnEggItem(CEHUSK, 0xe9967a, 0xa52a2a, (new Item.Properties()).group(ItemGroup.MISC)), CEZombieMod.CEHUSK_SPAWN_EGG_NAME)
            );
        }

        public static <T extends IForgeRegistryEntry<T>> T setup(final T entry, final String name) {
            return setup(entry, new ResourceLocation(CEZombieMod.MODID, name));
        }

        public static <T extends IForgeRegistryEntry<T>> T setup(final T entry, final ResourceLocation registryName) {
            entry.setRegistryName(registryName);
            return entry;
        }

    }

    private void registerSpawns() {
        Biome[] spawnBiomes = getAllSpawnBiomes();
        Biome[] desertBiomes = getBiomesFromTypes(BiomeDictionary.Type.HOT, BiomeDictionary.Type.DRY, BiomeDictionary.Type.SANDY);
        registerEntitySpawn(RegistryEvents.CEZOMBIE, spawnBiomes, ConfigHandler.CommonConfig.getCEZombieSpawnProb(), ConfigHandler.CommonConfig.getMinSpawn(), ConfigHandler.CommonConfig.getMaxSpawn());
        registerEntitySpawn(RegistryEvents.CEPIGZOMBIE, spawnBiomes, ConfigHandler.CommonConfig.getCEPigZombieSpawnProb(), ConfigHandler.CommonConfig.getMinSpawn(), ConfigHandler.CommonConfig.getMaxSpawn());
        registerEntitySpawn(RegistryEvents.CEHUSK, desertBiomes, ConfigHandler.CommonConfig.getCEHuskSpawnProb(), ConfigHandler.CommonConfig.getMinSpawn(), ConfigHandler.CommonConfig.getMaxSpawn());
    }

    private void registerEntitySpawn(EntityType<? extends LivingEntity> type, Biome[] biomes, int spawnProb, int minSpawn, int maxSpawn) {
        if (spawnProb <= 0) {
            // do not spawn this entity
            return;
        }

        for (Biome bgb : biomes) {
            Biome biome = ForgeRegistries.BIOMES.getValue(bgb.getRegistryName());
            if (biome != null) {
                biome.getSpawns(EntityClassification.MONSTER).add(new Biome.SpawnListEntry(type, spawnProb, minSpawn, maxSpawn));
            }
        }
    }

    public static Logger getLogger() {
        return LOGGER;
    }

    private Biome[] getAllSpawnBiomes() {
        LinkedList<Biome> list = new LinkedList<>();
        Collection<Biome> biomes = ForgeRegistries.BIOMES.getValues();

        for (Biome biome : biomes) {
            if (biome instanceof TheVoidBiome) {
                continue;
            }
            if (biome instanceof TheEndBiome) {
                continue;
            }
            if (biome instanceof NetherBiome) {
                continue;
            }
            if (biome instanceof DesertBiome || biome instanceof DesertHillsBiome || biome instanceof DesertLakesBiome) {
                continue;
            }
            if (isOceanTypeBiome(biome)) {
                continue;
            }
            if (!list.contains(biome)) {
                CEZombieMod.getLogger().info("Adding " + biome.getRegistryName() + " biome for spawning");
                list.add(biome);
            }
        }
        return list.toArray(new Biome[0]);
    }

    private boolean isOceanTypeBiome(Biome biome) {
        ResourceLocation txt = biome.getRegistryName();
        if (txt != null && txt.getPath().contains("ocean")) {
            CEZombieMod.getLogger().info("Excluding ocean biome: {}", txt);
            return true;
        }
        return false;
    }

    private Biome[] getBiomesFromTypes(BiomeDictionary.Type... types) {
        LinkedList<Biome> list = new LinkedList<>();
        Collection<Biome> biomes = ForgeRegistries.BIOMES.getValues();
        for (Biome biome : biomes) {
            int count = types.length;
            int shouldAdd = 0;
            for (BiomeDictionary.Type t : types) {
                if (BiomeDictionary.hasType(biome, t)) {
                    shouldAdd++;
                }
            }
            if (!list.contains(biome) && shouldAdd == count) {
                CEZombieMod.getLogger().info("Adding " + biome.getRegistryName() + " biome for spawning");
                list.add(biome);
            }
        }
        return list.toArray(new Biome[0]);
    }

}
