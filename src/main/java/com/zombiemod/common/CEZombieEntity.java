/*
 * CEZombieEntity.java
 *
 *  Copyright (c) 2019 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */
package com.zombiemod.common;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.TorchBlock;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.effect.LightningBoltEntity;
import net.minecraft.entity.merchant.villager.AbstractVillagerEntity;
import net.minecraft.entity.monster.CreeperEntity;
import net.minecraft.entity.monster.ZombieEntity;
import net.minecraft.entity.monster.ZombiePigmanEntity;
import net.minecraft.entity.passive.IronGolemEntity;
import net.minecraft.entity.passive.PigEntity;
import net.minecraft.entity.passive.TurtleEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.*;

import javax.annotation.Nonnull;

public class CEZombieEntity extends ZombieEntity {

    private static final DataParameter<Boolean> IS_GIANT = EntityDataManager.createKey(CEZombieEntity.class, DataSerializers.BOOLEAN);
    private static final DataParameter<Boolean> ARMS_RAISED = EntityDataManager.createKey(CEZombieEntity.class, DataSerializers.BOOLEAN);

    private final double noSpawnRadius = ConfigHandler.CommonConfig.getTorchNoSpawnRadius();

    private final float scaleFactor;

    public CEZombieEntity(EntityType<? extends CEZombieEntity> type, World world) {
        super(type, world);

        if (ConfigHandler.CommonConfig.allowGiantCEZombieSpawns() && (rand.nextInt(ConfigHandler.CommonConfig.getGiantCEZombieSpawnFactor()) == 1)) {
            scaleFactor = (float) ConfigHandler.CommonConfig.getGiantCEZombieScale();
            getDataManager().set(IS_GIANT, true);
        } else {
            scaleFactor = 1.0f;
            getDataManager().set(IS_GIANT, false);
        }
        EntitySize entitySize = new EntitySize(1.0f, 0.5f, false);
        entitySize.scale(0.6F * scaleFactor, 1.95F * scaleFactor);
        preventEntitySpawning = false;
    }

    @SuppressWarnings("unused")
    public CEZombieEntity(World world) {
        this(CEZombieMod.RegistryEvents.CEZOMBIE, world);
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    protected void applyEntityAI() {
        goalSelector.addGoal(2, new ZombieAttackGoal(this, 1.0D, false));
        goalSelector.addGoal(6, new MoveThroughVillageGoal(this, 1.0D, true, 4, this::isBreakDoorsTaskSet));
        goalSelector.addGoal(7, new WaterAvoidingRandomWalkingGoal(this, 1.0D));
        targetSelector.addGoal(1, (new HurtByTargetGoal(this)).setCallsForHelp(ZombiePigmanEntity.class));
        targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, PlayerEntity.class, true));
        targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, TurtleEntity.class, 10, true, false, TurtleEntity.TARGET_DRY_BABY));
        if (ConfigHandler.CommonConfig.getAttackVillagers()) {
            targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, AbstractVillagerEntity.class, true, true));
        }
        targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, IronGolemEntity.class, true));
        if (ConfigHandler.CommonConfig.getAttackPigs()) {
            targetSelector.addGoal(4, new NearestAttackableTargetGoal<>(this, PigEntity.class, true, true));
        }
    }

    @Override
    protected void registerAttributes() {
        super.registerAttributes();
        if (isGiant()) {
            getAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(ConfigHandler.CommonConfig.getAttackDamage() + 3);
        } else {
            getAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(ConfigHandler.CommonConfig.getAttackDamage());
        }
    }

    @Override
    protected void registerData() {
        super.registerData();
        getDataManager().register(IS_GIANT, false);
        getDataManager().register(ARMS_RAISED, false);
    }

    protected void registerGoals() {
        goalSelector.addGoal(4, new CEZombieEntity.AttackTurtleEggGoal(this, 1.0D, 3));
        goalSelector.addGoal(8, new LookAtGoal(this, PlayerEntity.class, 8.0F));
        goalSelector.addGoal(8, new LookRandomlyGoal(this));
        applyEntityAI();
    }

    public void setArmsRaised(boolean armsRaised) {
        getDataManager().set(ARMS_RAISED, armsRaised);
    }

    protected boolean shouldDrown() {
        return true;
    }

    public boolean isGiant() {
        return getDataManager().get(IS_GIANT);
    }

    protected boolean canBreakDoors() {
        return true;
    }

    @Override
    public boolean attackEntityFrom(@Nonnull DamageSource source, float amount) {
        return super.attackEntityFrom(source, amount);
    }

    @Override
    public void livingTick() {
        boolean burnFlag = shouldBurnInDay() && isInDaylight();
        if (burnFlag) {
            ItemStack itemstack = getItemStackFromSlot(EquipmentSlotType.HEAD);
            if (!itemstack.isEmpty()) {
                if (itemstack.isDamageable()) {
                    itemstack.setDamage(itemstack.getDamage() + rand.nextInt(2));
                    if (itemstack.getDamage() >= itemstack.getMaxDamage()) {
                        sendBreakAnimation(EquipmentSlotType.HEAD);
                        setItemStackToSlot(EquipmentSlotType.HEAD, ItemStack.EMPTY);
                    }
                }

                burnFlag = false;
            }

            if (burnFlag) {
                setFire(8);
            }
        }
        setArmsRaised(hasAttackableTarget());

        super.livingTick();
    }

    private boolean hasAttackableTarget() {
        LivingEntity entity = getAttackTarget();
        return ((entity instanceof PlayerEntity) || (entity instanceof AbstractVillagerEntity) || (entity instanceof IronGolemEntity) || (entity instanceof TurtleEntity));
    }

    protected boolean shouldBurnInDay() {
        return ConfigHandler.CommonConfig.getNightSpawnOnly();
    }

    // spawns on grass, sand, dirt, clay and occasionally spawn on stone unless
    // there are torches within the torch no-spawn radius, also won't spawn during
    // daytime if nightOnly flag is set.
    @Override
    public boolean canSpawn(@Nonnull IWorld world, @Nonnull SpawnReason spawnReason) {
        AxisAlignedBB entityAABB = getBoundingBox();

        if (noSpawnRadius > 0.0 && foundNearbyTorches(entityAABB)) {
            return false;
        } else if (ConfigHandler.CommonConfig.getNightSpawnOnly()) { // vanilla zombie spawn
            Vec3d v = getPositionVec();
            BlockPos blockpos = new BlockPos(v.x, entityAABB.minY, v.z);

            if (world.getLightFor(LightType.SKY, blockpos) > rand.nextInt(32)) {
                return false;
            } else {
                int lightFromNeighbors = world.getLight(blockpos);

                if (world.getWorld().isThundering()) {
                    int skylightSubtracted = world.getSkylightSubtracted();
                    world.getNeighborAwareLightSubtracted(blockpos, 10);
                    lightFromNeighbors = world.getWorld().getLight(blockpos);
                    world.getWorld().getNeighborAwareLightSubtracted(blockpos, skylightSubtracted);
                }

                return lightFromNeighbors <= rand.nextInt(8);
            }
        } else {
            boolean notColliding = world.hasNoCollisions(this, entityAABB);
            boolean isLiquid = world.containsAnyLiquid(entityAABB);
            // spawns on grass, sand, dirt, clay and very occasionally spawn on stone
            Vec3d v = getPositionVec();
            BlockPos bp = new BlockPos(v.x, entityAABB.minY - 1.0, v.z);
            Block block = world.getBlockState(bp).getBlock();
            boolean isGrass = (block == Blocks.GRASS);
            boolean isSand = (block == Blocks.SAND);
            boolean isClay = (block == Blocks.CLAY);
            boolean isDirt = (block == Blocks.DIRT);
            boolean isStone = (rand.nextBoolean()) && (block == Blocks.STONE);

            return (isGrass || isSand || isStone || isClay || isDirt) && notColliding && !isLiquid;
        }
    }

    // the aabb should be the entity's boundingbox
    public boolean foundNearbyTorches(AxisAlignedBB aabb) {
        boolean result = false;

        int xMin = MathHelper.floor(aabb.minX - noSpawnRadius);
        int xMax = MathHelper.floor(aabb.maxX + noSpawnRadius);
        int yMin = MathHelper.floor(aabb.minY - noSpawnRadius);
        int yMax = MathHelper.floor(aabb.maxY + noSpawnRadius);
        int zMin = MathHelper.floor(aabb.minZ - noSpawnRadius);
        int zMax = MathHelper.floor(aabb.maxZ + noSpawnRadius);

        for (int x = xMin; x <= xMax; x++) {
            for (int y = yMin; y <= yMax; y++) {
                for (int z = zMin; z <= zMax; z++) {
                    Block block = world.getBlockState(new BlockPos(x, y, z)).getBlock();
                    if (block instanceof TorchBlock) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    @Override
    public int getTotalArmorValue() {
        return super.getTotalArmorValue();
    }

    @Override
    public boolean canDespawn(double dist) {
        return false;
    }

    @Override
    public void tick() {
        super.tick();
    }

    @Override
    public void onStruckByLightning(@Nonnull LightningBoltEntity entityLightningBolt) {
        // A little surprise... BOOM!
        Vec3d v = getPositionVec();
        world.createExplosion(this, v.x, v.y, v.z, 0.5F, true, Explosion.Mode.DESTROY);
        dealFireDamage(5);
        setFire(8);
    }

    @Override
    protected SoundEvent getAmbientSound() {
        return SoundEvents.ENTITY_ZOMBIE_AMBIENT;
    }

    @Override
    protected SoundEvent getDeathSound() {
        return SoundEvents.ENTITY_ZOMBIE_DEATH;
    }

    @Nonnull
    @Override
    protected SoundEvent getStepSound() {
        return SoundEvents.ENTITY_ZOMBIE_STEP;
    }

    protected void playStepSound(@Nonnull BlockPos pos, @Nonnull BlockState blockIn) {
        playSound(getStepSound(), 0.15F, 1.0F);
    }

    @Nonnull
    @Override
    public CreatureAttribute getCreatureAttribute() {
        return CreatureAttribute.UNDEAD;
    }

    @Nonnull
    @Override
    protected ResourceLocation getLootTable() {
        return new ResourceLocation(CEZombieMod.MODID, CEZombieMod.CEZOMBIE_NAME);
    }

    @Override
    public void writeAdditional(CompoundNBT tagCompound) {
        super.writeAdditional(tagCompound);

    }

    @Override
    public void readAdditional(CompoundNBT tagCompund) {
        super.readAdditional(tagCompund);
    }

    @Override
    protected void setEquipmentBasedOnDifficulty(@Nonnull DifficultyInstance difficulty) {
        super.setEquipmentBasedOnDifficulty(difficulty);

        if (rand.nextFloat() < (world.getDifficulty() == Difficulty.HARD ? 0.05F : 0.01F)) {
            switch (rand.nextInt(4)) {
                case 0:
                    setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(Items.DIAMOND_SWORD));
                    break;
                case 1:
                    setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(Items.IRON_SWORD));
                    break;
                case 2:
                    setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(Items.DIAMOND_SHOVEL));
                    break;
                case 3:
                    setItemStackToSlot(EquipmentSlotType.MAINHAND, new ItemStack(Items.IRON_SHOVEL));
                    break;
            }
        }
    }

    @Override
    public void onKillEntity(@Nonnull LivingEntity entityLiving) {
        super.onKillEntity(entityLiving);
    }

    @Override
    protected void dropSpecialItems(@Nonnull DamageSource source, int looting, boolean recentlyHitIn) {
        super.dropSpecialItems(source, looting, recentlyHitIn);
        Entity entity = source.getTrueSource();
        if (entity instanceof CreeperEntity) {
            CreeperEntity creeperentity = (CreeperEntity) entity;
            if (creeperentity.ableToCauseSkullDrop()) {
                creeperentity.incrementDroppedSkulls();
                ItemStack itemstack = getSkullDrop();
                if (!itemstack.isEmpty()) {
                    entityDropItem(itemstack);
                }
            }
        }

    }

    @Nonnull
    @Override
    protected ItemStack getSkullDrop() {
        return new ItemStack(Items.ZOMBIE_HEAD);
    }

    class AttackTurtleEggGoal extends BreakBlockGoal {
        AttackTurtleEggGoal(CreatureEntity p_i50465_2_, double p_i50465_3_, int p_i50465_5_) {
            super(Blocks.TURTLE_EGG, p_i50465_2_, p_i50465_3_, p_i50465_5_);
        }

        public void playBreakingSound(IWorld p_203114_1_, @Nonnull BlockPos p_203114_2_) {
            p_203114_1_.playSound(null, p_203114_2_, SoundEvents.ENTITY_ZOMBIE_DESTROY_EGG, SoundCategory.HOSTILE, 0.5F, 0.9F + rand.nextFloat() * 0.2F);
        }

        public void playBrokenSound(World p_203116_1_, @Nonnull BlockPos p_203116_2_) {
            p_203116_1_.playSound(null, p_203116_2_, SoundEvents.ENTITY_TURTLE_EGG_BREAK, SoundCategory.BLOCKS, 0.7F, 0.9F + p_203116_1_.rand.nextFloat() * 0.2F);
        }

        public double getTargetDistanceSq() {
            return 1.14D;
        }
    }

}
