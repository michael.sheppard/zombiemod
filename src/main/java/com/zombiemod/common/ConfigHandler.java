/*
 * ConfigHandler.java
 *
 *  Copyright (c) 2019 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */
package com.zombiemod.common;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import net.minecraftforge.common.ForgeConfigSpec;
import org.apache.commons.lang3.tuple.Pair;

import java.nio.file.Paths;

public class ConfigHandler {

    public static void loadConfig() {
        CommentedFileConfig.builder(Paths.get("config", CEZombieMod.CEZOMBIE_NAME, CEZombieMod.MODID + ".toml")).build();
    }

    public static class CommonConfig {
        public static ForgeConfigSpec.IntValue cezombieSpawnProb;
        public static ForgeConfigSpec.IntValue cepigzombieSpawnProb;
        public static ForgeConfigSpec.IntValue cehuskSpawnProb;
        public static ForgeConfigSpec.IntValue giantCEZombieSpawnFactor;
        public static ForgeConfigSpec.DoubleValue giantCEZombieScale;
        public static ForgeConfigSpec.IntValue minSpawn;
        public static ForgeConfigSpec.IntValue maxSpawn;
        public static ForgeConfigSpec.DoubleValue torchNoSpawnRadius;
        public static ForgeConfigSpec.BooleanValue allowGiantZombieSpawns;
        public static ForgeConfigSpec.DoubleValue attackDamage;
        public static ForgeConfigSpec.BooleanValue attackPigs;
        public static ForgeConfigSpec.BooleanValue attackVillagers;
        public static ForgeConfigSpec.BooleanValue nightSpawnOnly;
        public static ForgeConfigSpec.BooleanValue denyVanillaZombieSpawns;
        public static ForgeConfigSpec.BooleanValue denyVanillaHuskSpawns;

        public CommonConfig(ForgeConfigSpec.Builder builder) {
            builder.comment(" CEZombie Config").push("CommonConfig");

            cezombieSpawnProb = builder
                    .comment("cezombieSpawnProb adjust to probability of zombies spawning",
                            "The higher the number the more likely cezombies will spawn."
                    )
                    .translation("config.cezombie.zombieSpawnProb")
                    .defineInRange("cezombieSpawnProb", 10, 0, 100);

            cepigzombieSpawnProb = builder
                    .comment("cepigzombieSpawnProb adjust to probability of cepigzombies spawning",
                            "The higher the number the more likely cepigzombies will spawn."
                    )
                    .translation("config.cezombie.pigzombieSpawnProb")
                    .defineInRange("cepigzombieSpawnProb", 10, 0, 100);

            cehuskSpawnProb = builder
                    .comment("cehuskSpawnProb adjust to probability of cehusks spawning",
                            "The higher the number the more likely cehusks will spawn."
                    )
                    .translation("config.cezombie.huskSpawnProb")
                    .defineInRange("cehuskSpawnProb", 10, 0, 100);

            giantCEZombieSpawnFactor = builder
                    .comment("giantcezombieSpawnFactor this factor determines chance of giantzombies spawning",
                            "The lower the number the more likely giantcezombies will spawn.")
                    .translation("config.cezombie.giantCEZombieSpawnFactor")
                    .defineInRange("giantCEZombieSpawnFactor", 30, 0, 100);

            giantCEZombieScale = builder
                    .comment("scale factor for the giant cezombie")
                    .translation("config.cezombie.giantCEZombieScale")
                    .defineInRange("giantCEZombieScale", 1.25, 1.00, 2.00);

            nightSpawnOnly = builder
                    .comment("Spawn  cezombies at night only")
                    .translation("config.cezombie.nightSpawnOnly")
                    .define("nightSpawnOnly", false);

            torchNoSpawnRadius = builder
                    .comment("set the radius in blocks for no spawning near torches, zero enables spawning near torches")
                    .translation("config.cezombie.torchNoSpawnRadius")
                    .defineInRange("torchNoSpawnRadius", 3.0, 0.0, 10.0);

            minSpawn = builder
                    .comment("minSpawn, minimum number of cezombies per spawn event")
                    .translation("config.cezombie.minSpawn")
                    .defineInRange("minSpawn", 1, 1, 4);

            maxSpawn = builder
                    .comment("maxSpawn, maximum number of cezombies per spawn event")
                    .translation("config.cezombie.maxSpawn")
                    .defineInRange("maxSpawn", 4, 1, 10);

            allowGiantZombieSpawns = builder
                    .comment("allow giantCEZombies to spawn")
                    .translation("config.cezombie.allowGiantCEZombieSpawns")
                    .define("allowGiantCEZombieSpawns", true);

            attackDamage = builder
                    .comment("set the initial attack damage caused by the cezombies")
                    .translation("config.cezombie.attackDamage")
                    .defineInRange("attackDamage", 3.0, 0.0, 5.0);

            attackPigs = builder
                    .comment("Attack and kill pigs")
                    .translation("config.cezombie.attackPigs")
                    .define("attackPigs", false);

            attackVillagers = builder
                    .comment("Attack and convert villagers")
                    .translation("config.cezombie.attackVillagers")
                    .define("attackVillagers", false);

            denyVanillaZombieSpawns = builder
                    .comment("allows/disallows vanilla zombies spawns, default is true,",
                            "no vanilla minecraft zombies will spawn. Only the " + CEZombieMod.CEZOMBIE_NAME + "s will spawn.")
                    .translation("config.cezombie.denyVanillaZombieSpawns")
                    .define("denyVanillaZombieSpawns", true);

            denyVanillaHuskSpawns = builder
                    .comment("allows/disallows vanilla husk spawns, default is true,",
                            "no vanilla minecraft husks will spawn. Only the " + CEZombieMod.CEHUSK_NAME + "s will spawn.")
                    .translation("config.cezombie.denyVanillaHuskSpawns")
                    .define("denyVanillaHuskSpawns", true);


            builder.pop();
        }

        public static int getCEZombieSpawnProb() { return cezombieSpawnProb.get(); }

        public static int getCEPigZombieSpawnProb() { return cepigzombieSpawnProb.get(); }

        public static int getCEHuskSpawnProb() { return cehuskSpawnProb.get(); }

        public static int getGiantCEZombieSpawnFactor() { return giantCEZombieSpawnFactor.get(); }

        public static double getGiantCEZombieScale() { return giantCEZombieScale.get(); }

        public static boolean getNightSpawnOnly() { return nightSpawnOnly.get(); }

        public static double getTorchNoSpawnRadius() { return torchNoSpawnRadius.get(); }

        public static int getMinSpawn() { return minSpawn.get(); }

        public static int getMaxSpawn() { return maxSpawn.get(); }

        public static boolean allowGiantCEZombieSpawns() { return allowGiantZombieSpawns.get(); }

        public static double getAttackDamage() { return attackDamage.get(); }

        public static boolean getAttackPigs() { return attackPigs.get(); }

        public static boolean getAttackVillagers() { return attackVillagers.get(); }

        public static boolean denyVanillaZombieSpawns() { return denyVanillaZombieSpawns.get(); }

        public static boolean denyVanillaHuskSpawns() { return denyVanillaHuskSpawns.get(); }
    }

    static final ForgeConfigSpec commonSpec;
    public static final CommonConfig COMMON_CONFIG;

    static {
        final Pair<CommonConfig, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(CommonConfig::new);
        commonSpec = specPair.getRight();
        COMMON_CONFIG = specPair.getLeft();
    }
}
